package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {

}
    

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
       grid.remove(row); // Er det meininga at vi ikkje skal bruke ArrayList-metoder når vi gjer dette?
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int firstRowSum = 0;
        int firstColSum = 0;
        int currentRowSum = 0;
        int currentColSum = 0;

        // Finner første radens sum
        for (int j = 0; j < grid.get(0).size(); j++) {
                firstRowSum += grid.get(0).get(j);
        }
    
        // Itererer over de neste radenes sum og ser om de matcher firstRow. Resetter så currentRowSum til 0.
        for (int i = 1; i < grid.size(); i++) {
            for (int j = 0; j < grid.get(i).size(); j++) {
                currentRowSum += grid.get(i).get(j);    
            } 
        
        if (firstRowSum != currentRowSum) {
            return false;
        }

        currentRowSum = 0;
        
        }
        
        // Finner summen av den første kolonnen
        for (int i = 0; i < grid.size(); i++) {
            firstColSum += grid.get(i).get(0);
        }

        // Itererer over de neste kolonnenes sum og ser om de matcher firstCol. Resetter så currentColSum til 0.
        for (int i = 1; i < grid.size(); i++) {
            for (int j = 0; j < grid.get(i).size(); j++) {
                currentColSum += grid.get(j).get(i);
            }
        
       if (firstColSum != currentColSum) {
            return false;
        }
        
        currentColSum = 0;
        }

        // Viss ingen av de øvrige betingelsene resulterer i false er vilkårene oppfyllt, og true returneres.
        return true;
    }

}